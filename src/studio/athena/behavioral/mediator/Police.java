package studio.athena.behavioral.mediator;

public class Police {
	public static void main(String[] args) {
		IntersectionAreaFacade intersection = new IntersectionAreaFacade();
		
		intersection.turnOn("vertical");
		intersection.turnOn("horizontal");
		intersection.turnOff("horizontal");
	}
}
