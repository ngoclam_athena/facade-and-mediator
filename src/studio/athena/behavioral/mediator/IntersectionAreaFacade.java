package studio.athena.behavioral.mediator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class IntersectionAreaFacade {
	List<AutoRotateTrafficLight> vertical_light = new ArrayList<AutoRotateTrafficLight>();
	List<AutoRotateTrafficLight> horizontal_light = new ArrayList<AutoRotateTrafficLight>();
	private final int YELLOW_DURATION = 1;
	
	public IntersectionAreaFacade() {
		horizontal_light.add(new AutoRotateTrafficLight("left", "horizontal"));
		horizontal_light.add(new AutoRotateTrafficLight("right", "horizontal"));
		vertical_light.add(new AutoRotateTrafficLight("front", "vertical"));
		vertical_light.add(new AutoRotateTrafficLight("back", "vertical"));
	}
	
	public void turnOn(String type) {
		System.out.println("=========TURN ON "+type+"===========");
		List<AutoRotateTrafficLight> on_list = type == "vertical" ? vertical_light : horizontal_light;
		List<AutoRotateTrafficLight> off_list = type == "vertical" ? horizontal_light : vertical_light;
		for (AutoRotateTrafficLight trafficlight : on_list) {
			System.out.println("---- " + trafficlight.position);
			trafficlight.turnGreen();
		}
		for (AutoRotateTrafficLight trafficlight : off_list) {
			System.out.println("---- " + trafficlight.position);
			trafficlight.turnRed();
		}
	}
	
	public void turnOff(String type) {
		System.out.println("=========TURN OFF "+type+"===========");
		List<AutoRotateTrafficLight> off_list = type == "vertical" ? vertical_light : horizontal_light;
		List<AutoRotateTrafficLight> on_list = type == "vertical" ? horizontal_light : vertical_light;
		for (AutoRotateTrafficLight trafficlight : on_list) {
			System.out.println("---- " + trafficlight.position);
			trafficlight.turnYellow();
		}
		try {
            TimeUnit.SECONDS.sleep(YELLOW_DURATION);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
		for (AutoRotateTrafficLight trafficlight : on_list) {
			System.out.println("---- " + trafficlight.position);
			trafficlight.turnRed();
		}
		for (AutoRotateTrafficLight trafficlight : off_list) {
			System.out.println("---- " + trafficlight.position);
			trafficlight.turnGreen();
		}
	}
}
