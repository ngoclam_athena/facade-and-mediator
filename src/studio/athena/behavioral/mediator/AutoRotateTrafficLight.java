package studio.athena.behavioral.mediator;


public class AutoRotateTrafficLight {
	public final String position ;
	public final String type;
	private LightMediator lightMediator = new LightMediator();
	Light[] lights = { new Light("Red", lightMediator),
						new Light("Yellow", lightMediator),
						new Light("Green", lightMediator)};
	public AutoRotateTrafficLight(String position, String type){
		this.position = position;
		this.type = type;
	}
	
	public void turnRed() {
		lights[0].turnOn();
	}
	
	public void turnYellow() {
		lights[1].turnOn();
	}
	
	public void turnGreen() {
		lights[2].turnOn();
	}
}
